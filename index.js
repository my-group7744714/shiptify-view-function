import { promises as fs } from 'fs';
import path from 'path';
import { parse } from '@babel/parser';
import _traverse from "@babel/traverse";
const traverse = _traverse.default;

const functionsJson = {};

async function extractFunctions(filePath) {
  const content = await fs.readFile(filePath, 'utf-8');
  const ast = parse(content, { sourceType: 'module' });

  const functionsJson = {};

  traverse(ast, {
    FunctionDeclaration(path) {
      const funcName = path.node.id ? path.node.id.name : 'anonymous';
      const params = path.node.params.map(param => param.name);

      functionsJson[funcName] = {
        params: params,
        return: '...', // Placeholder, add logic to determine the return value
        enterinFunction: extractNestedFunctions(path.node.body)
      };
    },
    ArrowFunctionExpression(path) {
      // Similar handling for arrow functions
    },
    // Additional visitors as needed
  });

  return functionsJson;
}

function extractNestedCalls(callNode, depth = 0) {
  let nestedCalls = [];

  if (callNode.type === 'CallExpression') {
    const functionName = callNode.callee.name;
    const args = callNode.arguments.map(arg => {
      // Обработка аргументов
      return arg.name || arg.value;
    });

    // Увеличиваем глубину рекурсии при каждом вложенном вызове
    const innerCalls = callNode.arguments
      .filter(arg => arg.type === 'CallExpression')
      .flatMap(arg => extractNestedCalls(arg, depth + 1));

    if (depth === 0) { // Добавляем информацию о вызове только на верхнем уровне
      nestedCalls.push({
        functionName: functionName,
        arguments: args,
        enterinFunction: innerCalls
      });
    }
  }

  return nestedCalls;
}


function extractNestedFunctions(node) {
  let nestedFunctions = [];

  // Traverse the node to find nested function declarations
  traverse(node, {
    FunctionDeclaration(innerPath) {
      const innerFuncName = innerPath.node.id ? innerPath.node.id.name : 'anonymous';
      const innerParams = innerPath.node.params.map(param => param.name);

      let innerNestedFunctions = extractNestedFunctions(innerPath.node.body);

      nestedFunctions.push({
        [innerFuncName]: {
          params: innerParams,
          return: '...', // Здесь должна быть логика для определения возвращаемого значения
          enterinFunction: innerNestedFunctions
        }
      });
    },
    CallExpression(innerPath) {
      let innerNestedFunctions;

      const calledFunctionName = innerPath.node.callee.name;
      const args = innerPath.node.arguments.map(arg => {
        if (arg.type === 'BinaryExpression') {
          return `${arg.left.name} ${arg.operator} ${arg.right.value}`;
        } else if (arg.type === 'CallExpression') {
          // Рекурсивно обрабатываем вложенный вызов функции
          return extractNestedCalls(arg);
        }
        return arg.name || arg.value;
      });

      if (innerPath.scope.block.type === 'FunctionDeclaration' || innerPath.scope.block.type === 'ArrowFunctionExpression') {
        innerNestedFunctions = extractNestedCalls(innerPath.node)
      }

      nestedFunctions.push({
        functionName: calledFunctionName,
        arguments: args,
        enterinFunction: innerNestedFunctions, // Собираем информацию о вложенных вызовах
      });
    },
    // Здесь можно добавить обработку ArrowFunctionExpression
  }, { scope: node.scope, state: node.state });
  console.log(nestedFunctions)
  return nestedFunctions;
}

async function processDirectory(directory) {
  const entries = await fs.readdir(directory, { withFileTypes: true });

  let allFunctions = {};

  for (let entry of entries) {
    const fullPath = path.join(directory, entry.name);

    if (entry.isDirectory()) {
      const subDirFunctions = await processDirectory(fullPath);
      allFunctions = { ...allFunctions, ...subDirFunctions };
    } else if (entry.isFile() && fullPath.endsWith('.js')) {
      const fileFunctions = await extractFunctions(fullPath);
      allFunctions[fullPath] = fileFunctions;
    }
  }

  return allFunctions;
}

async function main() {
  const directoryPath = './test/';
  const functionsJson = await processDirectory(directoryPath);
  console.log(JSON.stringify(functionsJson, null, 2));
}

main().catch(console.error);
